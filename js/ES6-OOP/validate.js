// Show thông báo chung
function showMessage(idSpan, message) {
  document.getElementById(idSpan).innerText = message;
}

// Kiểm tra không nhập
function checkEmpty(idSpan, value) {
  var length = value.trim().length;
  if (length != 0) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, "Không được để trống");
    return false;
  }
}

// Kiểm tra trùng mã
function checkExistId(ma) {
  let per = listPerson.getById(ma);
  if (!per) {
    showMessage("tbMa", "");
    return true;
  } else {
    showMessage("tbMa", "Mã đã tồn tại");
    return false;
  }
}

// Kiểm tra tên không được chứa số
function checkHoTen(value) {
  const regex = /^([^0-9]*)$/;
  var check = regex.test(value);
  if (check) {
    showMessage("tbHoTen", "");
    return true;
  } else {
    showMessage("tbHoTen", "Tên không được chứa số");
    return false;
  }
}

// Kiểm tra email
function checkEmail(email) {
  const regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var check = regex.test(email);
  if (check) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ");
    return false;
  }
}

// Kiểm tra giá trị số nhập vào (min <= number <= max)
function checkValueNumber(min, max, idSpan, number) {
  if (number >= min && number <= max) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, "Điểm từ 0 đến 10");
    return false;
  }
}
